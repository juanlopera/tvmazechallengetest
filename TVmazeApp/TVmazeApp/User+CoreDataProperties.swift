//
//  User+CoreDataProperties.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/4/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var password: String?
    @NSManaged public var username: String?
    @NSManaged public var userSeries: NSSet?

}

// MARK: Generated accessors for userSeries
extension User {

    @objc(addUserSeriesObject:)
    @NSManaged public func addToUserSeries(_ value: Series)

    @objc(removeUserSeriesObject:)
    @NSManaged public func removeFromUserSeries(_ value: Series)

    @objc(addUserSeries:)
    @NSManaged public func addToUserSeries(_ values: NSSet)

    @objc(removeUserSeries:)
    @NSManaged public func removeFromUserSeries(_ values: NSSet)

}
