//
//  Animation.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import UIKit

extension UIView {
    func animateTextFieldToCenter(with duration: TimeInterval) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: duration, animations: {
                self.frame = CGRect(x: (UIScreen.main.bounds.size.width / 2) - (self.frame.size.width / 2), y: self.frame.minY, width: self.frame.size.width, height: self.frame.size.height)
            })
        }
    }

    func animateLoginButton(with duration: TimeInterval) {
        DispatchQueue.main.async {
            UIView.animate(withDuration: duration, animations: {
                self.frame = CGRect(x: self.frame.minX, y: self.frame.minY - (UIScreen.main.bounds.height / 3), width: self.frame.size.width, height: self.frame.size.height)
            })
        }
    }
}
