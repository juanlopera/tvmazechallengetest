//
//  UserModel.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import Foundation

struct UserModel {
    var name: String?
    var password: String?
    var favSeries: SeriesModel?
}
