//
//  LoginViewController.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import UIKit

protocol LoginDisplayLogic: class {
    func showLoader(bool: Bool)
    func showErrorRequest(message: String)
    func showHome(userModel: UserModel)
}

class LoginViewController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var tvMazeTitleLabel: UILabel!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    //MARK:- Properties
    let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var router: LoginRouterLogic?
    var interactor: LoginInteractorBusinessLogic?
    
    private var userModel: UserModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDelegates()
        configureUI()
        animateLogin()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.Segue.loginToHome {
            if let homeVC = segue.destination as? HomeViewController {
                homeVC.userModel = sender as? UserModel
            }
        }
    }

    @IBAction func loginAction(_ sender: Any) {
        let username = usernameTextField.text!
        let password = passwordTextField.text!
        interactor?.fetchUser(username: username, password: password)
    }
    
    @IBAction func registerAction(_ sender: Any) {
        router?.showRegister()
    }
    
}

extension LoginViewController {
    private func setupDelegates() {
        let viewController = self
        let presenter = LoginPresenter()
        let interactor = LoginInteractor()
        let router = LoginRouter()
        viewController.router = router
        viewController.interactor = interactor
        presenter.loginVCDelegate = viewController
        interactor.presenterDelegate = presenter
        router.loginVC = viewController
    }
    
    private func configureUI() {
        //TextFields
        usernameTextField.frame = CGRect(x: -view.frame.size.width, y: (view.frame.size.height / 2) - 30,  width: view.frame.size.width * 0.75, height: 30)
        usernameTextField.placeholder = "username"
        
        passwordTextField.frame = CGRect(x: -view.frame.size.width, y: usernameTextField.frame.maxY + 30, width: view.frame.size.width * 0.75, height: 30)
        passwordTextField.placeholder = "password"
        
        //Buttons
        loginButton.frame = CGRect(x: (view.frame.size.width / 2) - ((view.frame.size.width * 0.45) / 2), y: view.frame.size.height, width: view.frame.size.width * 0.45, height: 40)
        loginButton.setTitle("Login", for: .normal)
        loginButton.backgroundColor = UIColor.greenTVmaze
        loginButton.setTitleColor(UIColor.white, for: .normal)
        
        registerButton.frame = CGRect(x: (view.frame.size.width / 2) - ((view.frame.size.width * 0.45) / 2), y: loginButton.frame.maxY + 25, width: view.frame.size.width * 0.45, height: 40)
        registerButton.setTitle("Register", for: .normal)
        registerButton.setTitleColor(UIColor.greenTVmaze, for: .normal)
        registerButton.backgroundColor = UIColor.white
        registerButton.layer.borderWidth = 1
        registerButton.layer.borderColor = UIColor.greenTVmaze.cgColor
        
        //Activity Indicator Loading
        activityIndicator.frame = CGRect(x: (view.frame.size.width / 2) - 20, y: view.frame.size.height / 2, width: 40, height: 40)
        activityIndicator.isHidden = true
        activityIndicator.color = UIColor.greenTVmaze
        view.addSubview(activityIndicator)
    }
    
    private func animateLogin() {
        usernameTextField.animateTextFieldToCenter(with: 2)
        passwordTextField.animateTextFieldToCenter(with: 2)
        loginButton.animateLoginButton(with: 2)
        registerButton.animateLoginButton(with: 2)
    }
    
    private func blockUIelements(bool: Bool) {
        usernameTextField.isEnabled = !bool
        passwordTextField.isEnabled = !bool
        loginButton.isEnabled = !bool
        registerButton.isEnabled = !bool
    }
}

extension LoginViewController: LoginDisplayLogic {
    func showLoader(bool: Bool) {
        switch bool {
        case true:
            DispatchQueue.main.async {
                self.blockUIelements(bool: true)
                self.activityIndicator.isHidden = false
                self.activityIndicator.startAnimating()
            }
        case false:
            DispatchQueue.main.async {
                self.blockUIelements(bool: false)
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    func showErrorRequest(message: String) {
        let errorAlert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let acceptAction = UIAlertAction(title: "OK", style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        errorAlert.addAction(acceptAction)
        present(errorAlert, animated: true, completion: nil)
    }
    
    func showHome(userModel: UserModel) {
        self.userModel = userModel
        usernameTextField.text = ""
        passwordTextField.text = ""
        router?.showHome(userMode: userModel)
    }
}

