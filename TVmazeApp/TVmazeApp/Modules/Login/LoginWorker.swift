//
//  LoginWorker.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import UIKit
import CoreData

class LoginWorker {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    func fetchUser(username: String, password: String, onSuccess: @escaping(UserModel) -> Void, onFaile: @escaping(String) -> Void) {
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Constants.CoreData.UserEntity.userEntity)
        fetchRequest.predicate = NSPredicate(format: "username == %@", username)
        do {
            let result = try managedContext.fetch(fetchRequest)
            var userResult: UserModel = UserModel(name: nil, password: nil, favSeries: nil)
            for data in result as! [NSManagedObject] {
                userResult.name = data.value(forKey: Constants.CoreData.UserEntity.usernameAttribute) as! String
                userResult.password = data.value(forKey: Constants.CoreData.UserEntity.passwordAttribute) as! String
            }
            if userResult.name != nil && userResult.password != nil {
                onSuccess(userResult)
            } else {
                onFaile("User not found, please check your username or password and try again")
            }
        } catch {
            onFaile("Something happend in our system, please try again")
        }
    }
}
