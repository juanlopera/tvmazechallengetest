//
//  LoginPresenter.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import Foundation

protocol LoginPresenterLogic: class {
    func showLoader(bool: Bool)
    func fetchUserResponse(userModel: UserModel, withUsername: String, withPassword: String)
    func errorOnRequest(errorMessage: String)
}

class LoginPresenter: LoginPresenterLogic {
    weak var loginVCDelegate: LoginDisplayLogic?

    func showLoader(bool: Bool) {
        loginVCDelegate?.showLoader(bool: bool)
    }

    func fetchUserResponse(userModel: UserModel, withUsername: String, withPassword: String) {
        if userModel.name != nil && userModel.password != nil {
            if userModel.name == withUsername && userModel.password == withPassword {
                loginVCDelegate?.showHome(userModel: userModel)
            } else {
                loginVCDelegate?.showErrorRequest(message: "Username or password incorrect, check it and try again")
            }
        }
    }

    func errorOnRequest(errorMessage: String) {
        loginVCDelegate?.showErrorRequest(message: errorMessage)
    }
}
