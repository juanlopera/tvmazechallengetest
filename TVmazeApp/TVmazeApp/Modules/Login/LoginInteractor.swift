//
//  LoginInteractor.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import Foundation

protocol LoginInteractorBusinessLogic {
    func fetchUser(username: String, password: String)
}

class LoginInteractor: LoginInteractorBusinessLogic {
    var presenterDelegate: LoginPresenterLogic?
    var worker: LoginWorker?

    func fetchUser(username: String, password: String) {
        presenterDelegate?.showLoader(bool: true)
        worker = LoginWorker()
        worker?.fetchUser(username: username, password: password, onSuccess: { (userModel) in
            self.presenterDelegate?.showLoader(bool: false)
            self.presenterDelegate?.fetchUserResponse(userModel: userModel, withUsername: username, withPassword: password)
        }, onFaile: { (errorMessage) in
            self.presenterDelegate?.showLoader(bool: false)
            self.presenterDelegate?.errorOnRequest(errorMessage: errorMessage)
        })
    }
}
