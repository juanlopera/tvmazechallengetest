//
//  LoginRouter.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import Foundation

protocol LoginRouterLogic {
    func showRegister()
    func showHome(userMode: UserModel)
}

class LoginRouter: LoginRouterLogic {
    var loginVC: LoginViewController?

    func showRegister() {
        loginVC?.performSegue(withIdentifier: Constants.Segue.loginToRegister, sender: nil)
    }

    func showHome(userMode: UserModel) {
        loginVC?.performSegue(withIdentifier: Constants.Segue.loginToHome, sender: userMode)
    }
}
