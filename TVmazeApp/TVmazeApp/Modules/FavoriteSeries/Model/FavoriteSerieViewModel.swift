//
//  FavoriteSerieViewModel.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/4/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import Foundation

struct FavoriteSerieViewModel {
    var id: String
    var img: NSData?
    var name: String
}
