//
//  FavoriteSeriesViewController.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/4/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import UIKit

protocol FavoriteDisplayLogic: class {
    func showLoader(bool: Bool)
    func showErrorRequest(message: String)
    func showSerieHeader(favSerieHeader: [FavoriteSerieViewModel])
    func showSerieBody(favSerieBody: FavoriteSerieDetailViewModel)
    func showNotFavoriteSeries(message: String)
    func showEpisodes(episodes: [EpisodeViewModel])
    func showSerieDeleted(message: String)
}

class FavoriteSeriesViewController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var seriesCollectionView: UICollectionView!
    @IBOutlet weak var seriesDetailTableView: UITableView!
    @IBOutlet weak var deleteSerieButton: UIButton!

    
    //MARK:- Properties
    var userModel: UserModel?
    private var indexHandlerCollection: Int = 0
    private var episodes: [EpisodeViewModel] = [EpisodeViewModel]()
    private let activityIndicator = UIActivityIndicatorView()
    private var favSerieHeader: [FavoriteSerieViewModel] = [FavoriteSerieViewModel]()
    private var favSerieBody: FavoriteSerieDetailViewModel?

    private var interactor: FavoriteInteractorBusinessLogic?
    private var router: FavoriteSerieRouterLogic?

    //CollectionView
    private let datasourceCollectionView = DatasourceFavoriteSerieCollectionView()
    private let delegateCollectionView = DelegateFavoriteSerieCollectionView()
    private let favSerieCollectionCellNibName = UINib(nibName: Constants.FavoriteSerieCollectionView.NIB.FavoriteSerieCellNibName, bundle: nil)

    //TableView
    private let datasourceTableView = DatasourceFavoriteSerieTableView()
    private let delegateTableView = DelegateFavoriteSerieTableView()
    private let favSerieTableCellNibName = UINib(nibName: Constants.SerieDetailTableView.NIB.genericCellNibName, bundle: nil)
    private let favSerieEpisodeCellNib = UINib(nibName: Constants.SerieDetailTableView.NIB.episodeCellNibName, bundle: nil)


    override func viewDidLoad() {
        super.viewDidLoad()
        setupDelegates()
        configureUI()
        registerCollectionCell()
        registerTableCell()
        interactor?.fetchFavoriteSeries(user: userModel!)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.Segue.favoriteToEpisodeDetail {
            if let episodeVC = segue.destination as? EpisodeDetailViewController {
                episodeVC.episodeViewModel = sender as! EpisodeViewModel
            }
        }
    }
    
    @IBAction func deleteSerieAction(_ sender: Any) {
        interactor?.deleteFavSerie(by: favSerieHeader[indexHandlerCollection].id)
        indexHandlerCollection = 0
    }
}

extension FavoriteSeriesViewController {
    private func setupDelegates() {
        let viewController = self
        let presenter = FavoritePresenter()
        let interactor = FavoriteInteractor()
        let router = FavoriteSerieRouter()
        viewController.interactor = interactor
        viewController.router = router
        presenter.favoriteVCDelegate = self
        interactor.presenter = presenter
        router.favoriteVCDelegate = viewController

        //CollectionView
        datasourceCollectionView.favSerieViewModel = favSerieHeader
        delegateCollectionView.delegate = self
        seriesCollectionView.dataSource = datasourceCollectionView
        seriesCollectionView.delegate = delegateCollectionView

        //TableView
        delegateTableView.delegate = self
        seriesDetailTableView.dataSource = datasourceTableView
        seriesDetailTableView.delegate = delegateTableView
    }

    private func configureUI() {
        //Activity Indicator Loading
        activityIndicator.frame = CGRect(x: (view.frame.size.width / 2) - 20, y: view.frame.size.height / 2, width: 40, height: 40)
        activityIndicator.isHidden = true
        activityIndicator.color = UIColor.greenTVmaze
        activityIndicator.backgroundColor = UIColor.grayTVmaze
        view.addSubview(activityIndicator)

        //Delete Button
        deleteSerieButton.layer.borderColor = UIColor.redDeleteTVmaze.cgColor
        deleteSerieButton.layer.borderWidth = 1
        deleteSerieButton.backgroundColor = UIColor.white
        deleteSerieButton.setTitle("Delete Serie", for: .normal)
        deleteSerieButton.setTitleColor(UIColor.redDeleteTVmaze, for: .normal)

        //TableView
        seriesDetailTableView.tableFooterView = UIView()
        seriesDetailTableView.estimatedRowHeight = 80
        seriesDetailTableView.rowHeight = UITableView.automaticDimension
    }

    private func registerCollectionCell() {
        seriesCollectionView.register(favSerieCollectionCellNibName, forCellWithReuseIdentifier: Constants.FavoriteSerieCollectionView.CellIdentifier.favoriteSerieCellIdentifier)
    }

    private func registerTableCell() {
        seriesDetailTableView.register(favSerieTableCellNibName, forCellReuseIdentifier: Constants.SerieDetailTableView.CellIdentifiers.genericSerieCell)
        seriesDetailTableView.register(favSerieEpisodeCellNib, forCellReuseIdentifier: Constants.SerieDetailTableView.CellIdentifiers.episodeDetailCell)
    }

    private func blockUIElements(bool: Bool) {
        seriesDetailTableView.isScrollEnabled = !bool
        seriesCollectionView.isScrollEnabled = !bool
        seriesCollectionView.isUserInteractionEnabled = !bool
        deleteSerieButton.isEnabled = !bool
    }
}

extension FavoriteSeriesViewController: FavoriteDisplayLogic {
    func showLoader(bool: Bool) {
        switch bool {
        case true:
            DispatchQueue.main.async {
                self.activityIndicator.isHidden = false
                self.activityIndicator.startAnimating()
            }
        case false:
            DispatchQueue.main.async {
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
            }
        }
    }

    func showErrorRequest(message: String) {
        let errorAlert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let acceptAction = UIAlertAction(title: "OK", style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        errorAlert.addAction(acceptAction)
        present(errorAlert, animated: true, completion: nil)
    }

    func showSerieDeleted(message: String) {
        let errorAlert = UIAlertController(title: "Success", message: message, preferredStyle: .alert)
        let acceptAction = UIAlertAction(title: "OK", style: .default) { (action) in
            self.interactor?.fetchFavoriteSeries(user: self.userModel!)
        }
        errorAlert.addAction(acceptAction)
        present(errorAlert, animated: true, completion: nil)
    }

    func showNotFavoriteSeries(message: String) {
        let errorAlert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let acceptAction = UIAlertAction(title: "OK", style: .default) { (action) in
            _ = self.navigationController?.popViewController(animated: true)
        }
        errorAlert.addAction(acceptAction)
        present(errorAlert, animated: true, completion: nil)
    }

    func showSerieHeader(favSerieHeader: [FavoriteSerieViewModel]) {
        self.favSerieHeader = favSerieHeader
        datasourceCollectionView.favSerieViewModel = favSerieHeader
        seriesCollectionView.reloadData()
        interactor?.fetchSerieDetail(index: indexHandlerCollection)
    }

    func showSerieBody(favSerieBody: FavoriteSerieDetailViewModel) {
        self.favSerieBody = favSerieBody
        datasourceTableView.favSerieDetail = self.favSerieBody
        interactor?.fetchEpisodes(by: self.favSerieBody!.id)
        seriesDetailTableView.reloadData()
    }

    func showEpisodes(episodes: [EpisodeViewModel]) {
        self.episodes = episodes
        self.favSerieBody!.episodes = self.episodes
        datasourceTableView.favSerieDetail = self.favSerieBody

        DispatchQueue.main.async {
            self.seriesDetailTableView.reloadData()
        }
    }
}

//CollectionView Protocol
extension FavoriteSeriesViewController: DelegateFavSerieDetailCollectionProtocol {
    func favSerieSelected(index: Int) {
        indexHandlerCollection = index
        interactor?.fetchSerieDetail(index: indexHandlerCollection)
    }
}


//TableView protocol
extension FavoriteSeriesViewController: DelegateFavSerieDetailTableProtocol {
    func favEpisodeSelected(index: Int) {
        router?.showEpisodeDetail(episodeDetail: self.favSerieBody!.episodes![index])
    }
}
