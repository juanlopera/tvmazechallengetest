//
//  FavoritePresenter.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/4/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import Foundation

protocol FavoritePresenterLogic {
    func favoriteSerieResponsePresenter(series: [Series])
    func favoriteSerieDetailResponsePresenter(series: Series)
    func showLoader(bool: Bool)
    func errorOnRequest(errorMessage: String)
    func episodesResponsePresenter(episodes: [EpisodeModel])
    func deleteSeriePresenter(message: String)
}

class FavoritePresenter: FavoritePresenterLogic {

    weak var favoriteVCDelegate: FavoriteDisplayLogic?

    func favoriteSerieResponsePresenter(series: [Series]) {
        var favSerieHeaderViewModel: [FavoriteSerieViewModel] = [FavoriteSerieViewModel]()
        for serie in series {
            var imgData: NSData? = nil
            if serie.image != nil {
                imgData = serie.image
            }
            let favSerie = FavoriteSerieViewModel(id: serie.id!, img: imgData, name: serie.name!)
            favSerieHeaderViewModel.append(favSerie)
        }
        if favSerieHeaderViewModel.count > 0 {
            favoriteVCDelegate?.showLoader(bool: false)
            favoriteVCDelegate?.showSerieHeader(favSerieHeader: favSerieHeaderViewModel)
        } else {
            favoriteVCDelegate?.showLoader(bool: false)
            favoriteVCDelegate?.showNotFavoriteSeries(message: "You don't have any favorite serie yet")
        }
    }

    func favoriteSerieDetailResponsePresenter(series: Series) {
        let favSerieDetail = FavoriteSerieDetailViewModel(id: series.id!, summary: series.summary!, episodes: nil)
        favoriteVCDelegate?.showLoader(bool: false)
        favoriteVCDelegate?.showSerieBody(favSerieBody: favSerieDetail)
    }

    func showLoader(bool: Bool) {
        favoriteVCDelegate?.showLoader(bool: bool)
    }

    func errorOnRequest(errorMessage: String) {
        favoriteVCDelegate?.showLoader(bool: false)
        favoriteVCDelegate?.showErrorRequest(message: errorMessage)
    }

    func episodesResponsePresenter(episodes: [EpisodeModel]) {
        var episodesViewModel = [EpisodeViewModel]()
        for episode in episodes {
            let episodeNumber = String(episode.number)
            let epidoseSeasson = String(episode.season)
            let imgURL = URL(string: episode.image.original)
            let imgData = try? Data(contentsOf: imgURL!)
            let newEpidose = EpisodeViewModel(name: episode.name, episodeNumber: episodeNumber, season: epidoseSeasson, summary: episode.summary, episodeImage: imgData!)
            episodesViewModel.append(newEpidose)
        }
        if episodesViewModel.count > 0 {
            favoriteVCDelegate?.showLoader(bool: false)
            favoriteVCDelegate?.showEpisodes(episodes: episodesViewModel)
        } else {
            favoriteVCDelegate?.showLoader(bool: false)
            favoriteVCDelegate?.showErrorRequest(message: "Episodes not found or not available in the moment, for more information contact TV Maze support")
        }
    }

    func deleteSeriePresenter(message: String) {
        favoriteVCDelegate?.showLoader(bool: false)
        favoriteVCDelegate?.showSerieDeleted(message: message)
    }
}
