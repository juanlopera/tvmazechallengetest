//
//  DatasourceFavoriteSerieCollectionView.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/4/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import UIKit

class DatasourceFavoriteSerieCollectionView: NSObject, UICollectionViewDataSource {

    var favSerieViewModel: [FavoriteSerieViewModel] = [FavoriteSerieViewModel]()

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return favSerieViewModel.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if favSerieViewModel.count > 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.FavoriteSerieCollectionView.CellIdentifier.favoriteSerieCellIdentifier, for: indexPath) as! FavoriteSerieCollectionViewCell
            cell.favSerieNameLabel.text = favSerieViewModel[indexPath.row].name
            cell.favSerieImage.image = UIImage(named: "tvmazeIMG")
            cell.contentView.layer.borderColor = UIColor.grayLineTVmaze.cgColor
            cell.contentView.layer.borderWidth = 1
//            if let image = UIImage(data: favSerieViewModel[indexPath.row].img as Data) {
//                cell.favSerieImage.image = image
//            }
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
}
