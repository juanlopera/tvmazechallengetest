//
//  DelegateFavoriteSerieCollectionView.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/4/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import UIKit

protocol DelegateFavSerieDetailCollectionProtocol {
    func favSerieSelected(index: Int)
}

class DelegateFavoriteSerieCollectionView: NSObject, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    var delegate: DelegateFavSerieDetailCollectionProtocol?

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.favSerieSelected(index: indexPath.item)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: 100)
    }
}
