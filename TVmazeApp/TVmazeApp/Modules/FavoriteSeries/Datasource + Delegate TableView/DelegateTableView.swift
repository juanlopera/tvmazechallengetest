//
//  DelegateTableView.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/5/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import UIKit

protocol DelegateFavSerieDetailTableProtocol {
    func favEpisodeSelected(index: Int)
}

class DelegateFavoriteSerieTableView: NSObject, UITableViewDelegate {

    var delegate: DelegateFavSerieDetailTableProtocol?

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.favEpisodeSelected(index: indexPath.row)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableView.automaticDimension
    }
}
