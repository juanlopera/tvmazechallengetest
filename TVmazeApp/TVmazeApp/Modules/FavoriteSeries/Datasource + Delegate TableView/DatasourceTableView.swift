//
//  DatasourceTableView.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/5/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import UIKit

class DatasourceFavoriteSerieTableView: NSObject, UITableViewDataSource {

    var favSerieDetail: FavoriteSerieDetailViewModel?

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard favSerieDetail != nil, favSerieDetail?.episodes != nil else {
            return 0
        }
        if section == 0 {
            return 1
        } else {
            return favSerieDetail!.episodes!.count
        }

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard favSerieDetail != nil, favSerieDetail?.episodes != nil else {
            return UITableViewCell()
        }
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.SerieDetailTableView.CellIdentifiers.genericSerieCell, for: indexPath) as! GenericDetailTableViewCell
            cell.genericTitleLabel.text = "Summary"
            cell.genericDetailLabel.text = favSerieDetail!.summary
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.SerieDetailTableView.CellIdentifiers.episodeDetailCell, for: indexPath) as! EpisodeTableViewCell
            cell.leftContainerView.isHidden = false
            cell.splitLineView.isHidden = false
            cell.nameLabel.text = "Epidose name"
            cell.episodeNameLabel.text = favSerieDetail!.episodes![indexPath.row].name
            cell.episodeNumberLabel.text = "Number"
            cell.numberEpisodeLabel.text = favSerieDetail!.episodes![indexPath.row].episodeNumber
            cell.episodeSeasonLabel.text = "Season: \(favSerieDetail!.episodes![indexPath.row].season)"
            cell.accessoryType = .disclosureIndicator
            return cell
        }
    }
}
