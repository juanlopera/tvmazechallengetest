//
//  FavoriteSerieRouter.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/5/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import Foundation

protocol FavoriteSerieRouterLogic {
    func showEpisodeDetail(episodeDetail: EpisodeViewModel)
}


class FavoriteSerieRouter: FavoriteSerieRouterLogic {
    var favoriteVCDelegate: FavoriteSeriesViewController?

    func showEpisodeDetail(episodeDetail: EpisodeViewModel) {
        favoriteVCDelegate?.performSegue(withIdentifier: Constants.Segue.favoriteToEpisodeDetail, sender: episodeDetail)
    }
}
