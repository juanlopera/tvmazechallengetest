//
//  FavoriteSerieWorker.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/4/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import UIKit
import CoreData

class FavoriteSerieWorker {

    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    private var seriesEntity: [Series] = [Series]()
    private let defaultSession = URLSession(configuration: .default)
    private var simpleTaskRequest: URLSessionDataTask?

    func fetchFavoriteSeries(user: UserModel, onSuccess: @escaping([Series]) -> Void, onFaile: @escaping(String) -> Void) {
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<Series>(entityName: Constants.CoreData.SerieEntity.serieEntity)
        fetchRequest.predicate = NSPredicate(format: "seriesUser.username == %@", user.name!)
        do {
            seriesEntity = try managedContext.fetch(fetchRequest)
            onSuccess(seriesEntity)
        } catch {
            onFaile("We got a error while we were trying to get your favorite series")
        }
    }

    func deleteFavSerie(with id: String, onResult: @escaping(Bool, String) -> Void) {
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<Series>(entityName: Constants.CoreData.SerieEntity.serieEntity)
        fetchRequest.predicate = NSPredicate(format: "id == %@", id)
        do {
            let result = try managedContext.fetch(fetchRequest)
            let serieToDelete = result[0] as NSManagedObject
            managedContext.delete(serieToDelete)
            do {
                try managedContext.save()
                onResult(true, "Serie deleted success")
            } catch {
                onResult(false, "Error saving changes")
            }
        } catch {
            onResult(false, "Error deleting your favorite serie")
        }
    }
    
    func fetchEpisode(by number: String, onSuccess: @escaping([EpisodeModel]) -> Void, onFaile: @escaping(String) -> Void){
        simpleTaskRequest?.cancel()

        if var urlComponents = URLComponents(string: "http://api.tvmaze.com/shows/\(number)/episodes") {
            guard let url = urlComponents.url else {
                onFaile("There was a error in our system while we were trying to get the episodes, please contact TV Maze support, or try again")
                return
            }
            simpleTaskRequest = defaultSession.dataTask(with: url, completionHandler: { (data, response, error) in
                defer {
                    self.simpleTaskRequest = nil
                }
                guard (error == nil) else {
                    onFaile("There was a error in our system while we were trying to get the episodes, please contact TV Maze support, or try again")
                    return
                }

                if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                    do {
                        let episodes = try JSONDecoder().decode([EpisodeModel].self, from: data)
                        onSuccess(episodes)
                    } catch {
                        onFaile("We got a error with data format, please try again or contact with TV Maze support")
                    }
                } else {
                    onFaile("There was a error in our system while we were trying to get the episodes, please contact TV Maze support, or try again")
                }
            })
            simpleTaskRequest?.resume()
        }
    }
}
