//
//  FavoriteSerieCollectionViewCell.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/4/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import UIKit

class FavoriteSerieCollectionViewCell: UICollectionViewCell {

    //MARK:- IBOutlet
    @IBOutlet weak var favSerieImage: UIImageView!
    @IBOutlet weak var favSerieNameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
