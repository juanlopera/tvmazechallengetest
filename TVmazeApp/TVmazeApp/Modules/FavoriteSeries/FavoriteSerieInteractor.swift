//
//  FavoriteSerieInteractor.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/4/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import Foundation

protocol FavoriteInteractorBusinessLogic {
    func fetchFavoriteSeries(user: UserModel)
    func fetchSerieDetail(index: Int)
    func fetchEpisodes(by showNumber: String)
    func deleteFavSerie(by id: String)
}

class FavoriteInteractor: FavoriteInteractorBusinessLogic {
    
    var presenter: FavoritePresenterLogic?
    var worker: FavoriteSerieWorker?

    private var series: [Series] = [Series]()

    func fetchFavoriteSeries(user: UserModel) {
        presenter?.showLoader(bool: true)
        worker = FavoriteSerieWorker()
        worker?.fetchFavoriteSeries(user: user, onSuccess: { (series) in
            self.series = series
            self.presenter?.favoriteSerieResponsePresenter(series: series)
        }, onFaile: { (errorMessage) in
            self.presenter?.errorOnRequest(errorMessage: errorMessage)
        })
    }

    func fetchEpisodes(by showNumber: String) {
        presenter?.showLoader(bool: true)
        if showNumber.isEmpty {
            presenter?.errorOnRequest(errorMessage: "The episode is invalid or not available in the moment")
        } else {
            worker = FavoriteSerieWorker()
            worker?.fetchEpisode(by: showNumber, onSuccess: { (episodes) in
                self.presenter?.episodesResponsePresenter(episodes: episodes)
            }, onFaile: { (errorMessage) in
                self.presenter?.errorOnRequest(errorMessage: errorMessage)
            })
            worker = nil
        }
    }

    func deleteFavSerie(by id: String) {
        presenter?.showLoader(bool: true)
        if id.isEmpty {
            presenter?.errorOnRequest(errorMessage: "Error getting the serie ID")
        } else {
            worker = FavoriteSerieWorker()
            worker?.deleteFavSerie(with: id, onResult: { (bool, message) in
                switch bool {
                case true:
                    self.presenter?.deleteSeriePresenter(message: message)
                case false:
                    self.presenter?.errorOnRequest(errorMessage: message)
                }
            })
            worker = nil
        }
    }

    func fetchSerieDetail(index: Int) {
        presenter?.showLoader(bool: true)
        presenter?.favoriteSerieDetailResponsePresenter(series: series[index])
    }
}

