//
//  EpisodeDetailViewController.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/4/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import UIKit

class EpisodeDetailViewController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var headerContainerView: UIView!
    @IBOutlet weak var epidoseImage: UIImageView!
    @IBOutlet weak var episodeNameLabel: UILabel!
    @IBOutlet weak var bodyContainerView: UIView!
    @IBOutlet weak var episodeSeasonNumberLabel: UILabel!
    @IBOutlet weak var episodeNumberLabel: UILabel!
    @IBOutlet weak var footerContainerView: UIView!
    @IBOutlet weak var episodeSummary: UITextView!
    
    //MARK:- Properties
    var episodeViewModel: EpisodeViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = UIColor.greenTVmaze
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        configureUI()
    }

}

extension EpisodeDetailViewController {
    private func configureUI() {
        if let episode = episodeViewModel {
            //Header Container
            putShowToView(viewContext: headerContainerView)
            epidoseImage.image = UIImage(data: episode.episodeImage)
            episodeNameLabel.text = episode.name

            //Body Container
            putShowToView(viewContext: bodyContainerView)
            episodeSeasonNumberLabel.text = episode.season
            episodeNumberLabel.text = episode.episodeNumber

            //Footer Container
            putShowToView(viewContext: footerContainerView)
            episodeSummary.text = episode.summary
        }
    }

    private func putShowToView(viewContext: UIView) {
        viewContext.layer.borderWidth = 1
        viewContext.layer.borderColor = UIColor.grayLineTVmaze.cgColor
        viewContext.layer.cornerRadius = 5
        viewContext.layer.masksToBounds = false
        viewContext.layer.shadowColor = UIColor.grayTVmaze.cgColor
        viewContext.layer.shadowOpacity = 0.8
        viewContext.layer.shadowRadius = 5
        viewContext.layer.shadowOffset = CGSize(width: 5, height: -4)
    }
}
