//
//  HomeRouter.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import Foundation

protocol HomeRouterLogic {
    func showSerieDetail(serieViewModel: SerieViewModel)
    func showFavorites()
}

class HomeRouter: HomeRouterLogic {
    var homeVCDelegate: HomeViewController?
    
    func showSerieDetail(serieViewModel: SerieViewModel) {
        homeVCDelegate?.performSegue(withIdentifier: Constants.Segue.homeToSerieDetail, sender: serieViewModel)
    }

    func showFavorites() {
        homeVCDelegate?.performSegue(withIdentifier: Constants.Segue.homeToFavorites, sender: nil)
    }

}
