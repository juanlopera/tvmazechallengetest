//
//  DelegateCollectionView.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import UIKit
protocol DelegateSeriesCollectionProtocol {
    func serieSelected(index: Int)
}

class DelegateCollectionView: NSObject, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    var series: [SerieViewModel] = [SerieViewModel]()
    var delegate: DelegateSeriesCollectionProtocol?

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.serieSelected(index: indexPath.row)
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.alpha = 0.0
        UIView.animate(withDuration: 1.0) {
            cell.alpha = 1.0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if series.count > 0 {
            return CGSize(width: UIScreen.main.bounds.size.width * 0.80, height: 200.0)
        } else {
            return CGSize.zero
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 50.0
    }
}
