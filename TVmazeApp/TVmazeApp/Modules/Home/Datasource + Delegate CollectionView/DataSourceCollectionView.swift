//
//  DataSourceCollectionView.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import UIKit

class DataSourceCollectionView: NSObject, UICollectionViewDataSource {

    var series: [SerieViewModel] = [SerieViewModel]()

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return series.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.SeriesCollectionView.CellIdentifiers.seriesCellIdentifier, for: indexPath) as! SerieCollectionCell
        if series.count > 0 {
            cell.serieImage.image = UIImage(data: series[indexPath.row].image)
            cell.serieName.text = series[indexPath.row].name
            cell.summaryLabel.text = "Summary"
            cell.serieSummary.text = series[indexPath.row].summary
        }
        return cell
    }
}
