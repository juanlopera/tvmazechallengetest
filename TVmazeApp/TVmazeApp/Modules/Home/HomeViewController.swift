//
//  HomeViewController.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import UIKit

protocol HomeDisplayLogic: class {
    func showLoader(bool: Bool)
    func showErrorRequest(message: String)
    func showSeries(series: [SerieViewModel])
    func showAllSeriesButton(bool: Bool)
}

class HomeViewController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var seriesCollectionView: UICollectionView!
    @IBOutlet weak var allSeriesButton: UIButton!
    

    //MARK:- Properties
    var userModel: UserModel?
    private var router: HomeRouterLogic?
    private var interactor: HomeInteractorBusinessLogic?

    private let backButton = UIBarButtonItem()
    private let favoritesBarButton = UIBarButtonItem()
    private let activityIndicator = UIActivityIndicatorView()
    private let datasourceCollectionView = DataSourceCollectionView()
    private let delegateCollectionView = DelegateCollectionView()

    private var series: [SerieViewModel] = [SerieViewModel]()
    private let seriesCellNib = UINib(nibName: Constants.SeriesCollectionView.NIB.seriesNibName, bundle: nil)

    override func viewDidLoad() {
        super.viewDidLoad()
        backButton.title = "Log out"
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        self.navigationController?.navigationBar.barTintColor = UIColor.greenTVmaze
        self.navigationController?.navigationBar.tintColor = UIColor.white
        favoritesBarButton.title = "Favorites"
        self.navigationItem.rightBarButtonItem = favoritesBarButton
        favoritesBarButton.target = self
        favoritesBarButton.action = #selector(favorites)
        setupDelegates()
        configureUI()
        registerCollectionCell()
        interactor?.fetchSeries()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.Segue.homeToSerieDetail {
            if let serieDetailVC = segue.destination as? SerieDetailViewController {
                serieDetailVC.serieViewModel.removeAll()
                serieDetailVC.serieViewModel.append(sender as! SerieViewModel)
                serieDetailVC.userModel = self.userModel
                showLoader(bool: false)
            }
        } else if segue.identifier == Constants.Segue.homeToFavorites {
            if let favoritesVC = segue.destination as? FavoriteSeriesViewController {
                favoritesVC.userModel = self.userModel
            }
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    @IBAction func searchAction(_ sender: Any) {
        let serieToSearch = searchTextField.text!
        interactor?.fetchSeriesWith(name: serieToSearch)
    }
    
    @IBAction func allSeriesAction(_ sender: Any) {
        interactor?.fetchSeries()
    }
    
}

extension HomeViewController {
    private func setupDelegates() {
        let viewController = self
        let presenter = HomePresenter()
        let interactor = HomeInteractor()
        let router = HomeRouter()
        viewController.interactor = interactor
        viewController.router = router
        presenter.homeViewControllerDelegate = viewController
        interactor.presenterDelegate = presenter
        router.homeVCDelegate = viewController

        //CollectionView Datasource and Delegate
        seriesCollectionView.dataSource = datasourceCollectionView
        delegateCollectionView.delegate = self
        seriesCollectionView.delegate = delegateCollectionView
    }

    private func configureUI() {
        //Buttons
        searchButton.layer.borderColor = UIColor.greenTVmaze.cgColor
        searchButton.layer.borderWidth = 1
        searchButton.layer.cornerRadius = 5
        searchButton.backgroundColor = UIColor.white
        searchButton.setTitleColor(UIColor.greenTVmaze, for: .normal)
        searchButton.setTitle("Search", for: .normal)

        allSeriesButton.isEnabled = false
        allSeriesButton.isHidden = true

        //Activity Indicator Loading
        activityIndicator.frame = CGRect(x: (view.frame.size.width / 2) - 20, y: view.frame.size.height / 2, width: 40, height: 40)
        activityIndicator.isHidden = true
        activityIndicator.color = UIColor.greenTVmaze
        activityIndicator.backgroundColor = UIColor.grayTVmaze
        view.addSubview(activityIndicator)
    }

    private func registerCollectionCell() {
        seriesCollectionView.register(seriesCellNib, forCellWithReuseIdentifier: Constants.SeriesCollectionView.CellIdentifiers.seriesCellIdentifier)
    }

    private func blockUIelements(bool: Bool) {
        searchTextField.isEnabled = !bool
        searchButton.isEnabled = !bool
        favoritesBarButton.isEnabled = !bool
        seriesCollectionView.allowsSelection = !bool
        seriesCollectionView.isScrollEnabled = !bool
    }

    @objc private func favorites() {
        router?.showFavorites()
    }
}

extension HomeViewController: HomeDisplayLogic {

    func showSeries(series: [SerieViewModel]) {
        self.series = series
        datasourceCollectionView.series = self.series
        delegateCollectionView.series = self.series
        DispatchQueue.main.async {
            self.seriesCollectionView.reloadData()
        }
    }

    func showAllSeriesButton(bool: Bool) {
        DispatchQueue.main.async {
            self.allSeriesButton.isHidden = !bool
            self.allSeriesButton.isEnabled = bool
        }
    }

    func showLoader(bool: Bool) {
        switch bool {
        case true:
            DispatchQueue.main.async {
                self.blockUIelements(bool: true)
                self.activityIndicator.isHidden = false
                self.activityIndicator.startAnimating()
            }
        case false:
            DispatchQueue.main.async {
                self.blockUIelements(bool: false)
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
            }
        }
    }

    func showErrorRequest(message: String) {
        let errorAlert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let acceptAction = UIAlertAction(title: "OK", style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        errorAlert.addAction(acceptAction)
        present(errorAlert, animated: true, completion: nil)
    }
}

extension HomeViewController: DelegateSeriesCollectionProtocol {
    func serieSelected(index: Int) {
        self.showLoader(bool: true)
        router?.showSerieDetail(serieViewModel: self.series[index])
    }
}
