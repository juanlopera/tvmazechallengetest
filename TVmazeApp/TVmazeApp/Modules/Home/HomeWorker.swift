//
//  HomeWorker.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import Foundation

enum GenericsError: Error {
    case onResponse
    case onRequest
}

class HomeWorker {

    private let defaultSession = URLSession(configuration: .default)
    private var simpleTaskRequest: URLSessionDataTask?

    func fetchAllSeries(onResult: @escaping(Result<[ShowModel], GenericsError>) -> Void) {

        //If there is some request working that need to be cancel
        simpleTaskRequest?.cancel()

        if var urlComponents = URLComponents(string: "http://api.tvmaze.com/shows") {
            guard let url = urlComponents.url else {
                onResult(.failure(.onRequest))
                return
            }
            simpleTaskRequest =
                defaultSession.dataTask(with: url) { [weak self] data, response, error in
                    defer {
                        self?.simpleTaskRequest = nil
                    }
                    guard (error == nil) else {
                        onResult(.failure(.onRequest))
                        return
                    }

                    if let data = data, let response = response as? HTTPURLResponse,
                        response.statusCode == 200 {
                        do {
                            let shows = try JSONDecoder().decode([ShowModel].self, from: data)
                            onResult(.success(shows))
                        } catch {
                            onResult(.failure(.onResponse))
                        }
                    } else {
                        onResult(.failure(.onRequest))
                    }
            }
            simpleTaskRequest?.resume()
        }
    }

    func fetchSerieWith(name: String, onResult: @escaping(Result<ShowModel, GenericsError>) -> Void) {

        simpleTaskRequest?.cancel()
        if var urlComponents = URLComponents(string: "http://api.tvmaze.com/singlesearch/shows?q=\(name)") {
            guard let url = urlComponents.url else {
                onResult(.failure(.onRequest))
                return
            }
            simpleTaskRequest = defaultSession.dataTask(with: url, completionHandler: { (data, response, error) in
                defer {
                    self.simpleTaskRequest = nil
                }
                guard (error == nil) else {
                    onResult(.failure(.onRequest))
                    return
                }
                if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                    do {
                        let show = try JSONDecoder().decode(ShowModel.self, from: data)
                        onResult(.success(show))
                    } catch {
                        onResult(.failure(.onResponse))
                    }
                } else {
                    onResult(.failure(.onRequest))
                }
            })
            simpleTaskRequest?.resume()
        }
    }
}
