//
//  HomeInteractor.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import Foundation

protocol HomeInteractorBusinessLogic {
    var presenterDelegate: HomePresenterLogic? { get }
    func fetchSeries()
    func fetchSeriesWith(name: String)
}

class HomeInteractor: HomeInteractorBusinessLogic {

    var presenterDelegate: HomePresenterLogic?
    lazy var worker: HomeWorker = HomeWorker()

    var showsModel: [ShowModel] = [ShowModel]()
    
    func fetchSeries() {
        presenterDelegate?.showLoader(bool: true)
        worker.fetchAllSeries(onResult: { (result) in
            switch result {
            case .success(let showsModelResponse):
                self.showsModel = showsModelResponse
                self.presenterDelegate?.allSeriesResponsePresent(shows: self.caseShowSeries(showsModelResponse: showsModelResponse))
            case .failure(let showsError):
                self.caseError(error: showsError)
            }
        })

    }

    func fetchSeriesWith(name: String) {
        presenterDelegate?.showLoader(bool: true)
        if name.isEmpty {
            presenterDelegate?.emptySerieToShow()
        } else {
            worker.fetchSerieWith(name: name) { (result) in
                switch result {
                case .success(let showResponse):
                    self.presenterDelegate?.specificSerieResponsePresent(showModel: self.caseShowSpecificSerie(showsModelResponse: showResponse))
                case .failure(let error):
                    self.caseError(error: error)
                }
            }
        }
    }
}

extension HomeInteractor {
    private func caseError(error: GenericsError) {
        switch error {
        case .onRequest:
            self.presenterDelegate?.onResquestError(error: .onRequest)
        case .onResponse:
            self.presenterDelegate?.onResquestError(error: .onResponse)
        }
    }

    private func caseShowSeries(showsModelResponse: [ShowModel]) -> [SerieViewModel] {
        let seriesViewModel = showsModelResponse.map { (showModel) -> SerieViewModel in
            let id = String(showModel.id)
            let imgURL = URL(string: showModel.image.original)
            let imageData = try? Data(contentsOf: imgURL!)
            let serieVM = SerieViewModel(id: id, url: showModel.url, name: showModel.name, genres: showModel.genres, availableDays: showModel.schedule.days, startTime: showModel.schedule.time, image: imageData ?? Data(), summary: showModel.summary, episodes: nil)
            return serieVM
        }
        return seriesViewModel
    }

    private func caseShowSpecificSerie(showsModelResponse: ShowModel) -> SerieViewModel {
        let id = String(showsModelResponse.id)
        let imgURL = URL(string: showsModelResponse.image.original)
        let imageData = try? Data(contentsOf: imgURL!)
        return SerieViewModel(id: id, url: showsModelResponse.url, name: showsModelResponse.name, genres: showsModelResponse.genres, availableDays: showsModelResponse.schedule.days, startTime: showsModelResponse.schedule.time, image: imageData ?? Data(), summary: showsModelResponse.summary, episodes: nil)
    }
}
