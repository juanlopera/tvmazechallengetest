//
//  SerieCollectionCell.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import UIKit

class SerieCollectionCell: UICollectionViewCell {


    //MARK:- IBOutlet
    @IBOutlet weak var serieImage: UIImageView!
    @IBOutlet weak var serieName: UILabel!
    @IBOutlet weak var serieSummary: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var splitView: UIView!
    @IBOutlet weak var moreInformationContainer: UIView!


    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.layer.borderColor = UIColor.grayLineTVmaze.cgColor
        self.contentView.layer.borderWidth = 1
        self.contentView.layer.cornerRadius = 5
    }

}
