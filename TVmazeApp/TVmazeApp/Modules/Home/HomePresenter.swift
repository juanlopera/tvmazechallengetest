//
//  HomePresenter.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import Foundation

protocol HomePresenterLogic: class {
    var homeViewControllerDelegate: HomeDisplayLogic? { get }
    func showLoader(bool: Bool)
    func allSeriesResponsePresent(shows: [SerieViewModel])
    func specificSerieResponsePresent(showModel: SerieViewModel)
    func onResquestError(error: GenericsError)
    func onResponseError(error: GenericsError)
    func emptySerieToShow()
}

class HomePresenter: HomePresenterLogic {

    weak var homeViewControllerDelegate: HomeDisplayLogic?
    
    func allSeriesResponsePresent(shows: [SerieViewModel]) {
        if shows.count > 0 {
            var showsFormated = shows
            for i in 0...showsFormated.count - 1 {
                showsFormated[i].summary = Utils.fromHTMLFormatter(string: showsFormated[i].summary)
            }
            homeViewControllerDelegate?.showAllSeriesButton(bool: false)
            homeViewControllerDelegate?.showLoader(bool: false)
            homeViewControllerDelegate?.showSeries(series: showsFormated)
        } else {
            homeViewControllerDelegate?.showLoader(bool: false)
            homeViewControllerDelegate?.showErrorRequest(message: "Something happend in our system, can you try again")
        }
    }

    func specificSerieResponsePresent(showModel: SerieViewModel) {
        homeViewControllerDelegate?.showLoader(bool: false)
        homeViewControllerDelegate?.showAllSeriesButton(bool: true)
        homeViewControllerDelegate?.showSeries(series: [showModel])
    }

    func showLoader(bool: Bool) {
        homeViewControllerDelegate?.showLoader(bool: bool)
    }

    func onResquestError(error: GenericsError) {
        let errorMessage: String = "There is a problem with TV Maze system, please go to http://www.tvmaze.com/api   for more information"
        homeViewControllerDelegate?.showErrorRequest(message: errorMessage)
        homeViewControllerDelegate?.showLoader(bool: false)
    }

    func onResponseError(error: GenericsError) {
        let errorMessage: String = "Was a error in the format response with our system, please try again or contact TV Maze API for more information"
        homeViewControllerDelegate?.showErrorRequest(message: errorMessage)
        homeViewControllerDelegate?.showLoader(bool: false)
    }

    func emptySerieToShow() {
        let errorMessage: String = "Please, type a serie and after press the search button"
        homeViewControllerDelegate?.showErrorRequest(message: errorMessage)
        homeViewControllerDelegate?.showLoader(bool: false)
    }
}
