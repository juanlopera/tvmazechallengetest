//
//  SeriesModel.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import Foundation

struct ImageResponseModel: Decodable {
    var medium: String
    var original: String
}

struct ScheduleResponseModel: Decodable {
    var time: String
    var days: [String]
}
struct ShowModel: Decodable {
    var id: Int
    var url: String
    var name: String
    var genres: [String]
    var schedule: ScheduleResponseModel
    var image: ImageResponseModel
    var summary: String
//    var episodes: EpisodeModel
}
