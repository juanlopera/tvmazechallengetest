//
//  EpisodeViewModel.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import Foundation

struct EpisodeViewModel {
    var name: String
    var episodeNumber: String
    var season: String
    var summary: String
    var episodeImage: Data
}
