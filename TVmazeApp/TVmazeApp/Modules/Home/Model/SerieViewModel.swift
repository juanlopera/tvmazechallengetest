//
//  SerieViewModel.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import Foundation

struct SerieViewModel {
    var id: String
    var url: String
    var name: String
    var genres: [String]
    var availableDays: [String]
    var startTime: String
    var image: Data
    var summary: String
    var episodes: [EpisodeViewModel]?
}
