//
//  EpisodeModel.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import Foundation

struct EpisodeModel: Decodable {
    var id: Int
    var name: String
    var season: Int
    var number: Int
    var summary: String
    var image: ImageResponseModel
}
