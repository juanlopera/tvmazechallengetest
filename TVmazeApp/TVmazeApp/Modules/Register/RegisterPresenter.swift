//
//  RegisterPresenter.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import Foundation

protocol RegisterPresenterLogic: class {
    func creatingUSerResponse(isSuccess: Bool)
    func showLoader(bool: Bool)
}

class RegisterPresenter: RegisterPresenterLogic {
    weak var registerVCDelegate: RegisterDisplayLogic?

    func showLoader(bool: Bool) {
        registerVCDelegate?.showLoader(bool: bool)
    }

    func creatingUSerResponse(isSuccess: Bool) {
        switch isSuccess {
        case true:
            registerVCDelegate?.showUserCreated(message: "User created with success, now you can enjoy this app")
        case false:
            registerVCDelegate?.showError(message: "Was a problem while we were creating the user, check that the username, password and confirm password are ok and try again")
        }
    }
}
