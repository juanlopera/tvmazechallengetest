//
//  RegisterRouter.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import Foundation

protocol RegisterRouterLogic {
    func showLogin()
}

class RegisterRouter: RegisterRouterLogic {

    var registerVC: RegisterViewController?

    func showLogin() {
        _ = registerVC?.navigationController?.popViewController(animated: true)
    }
}
