//
//  RegisterViewController.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import UIKit

protocol RegisterDisplayLogic: class {
    func showLoader(bool: Bool)
    func showLogin()
    func showUserCreated(message: String)
    func showError(message: String)
    
}

class RegisterViewController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmationPasswordTextField: UITextField!
    @IBOutlet weak var createAccountButton: UIButton!

    //MARK:- Properties
    let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var router: RegisterRouterLogic?
    var interactor: RegisterInteractorBusinessLogic?

    override func viewDidLoad() {
        super.viewDidLoad()
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        self.navigationController?.navigationBar.barTintColor = UIColor.greenTVmaze
        self.navigationController?.navigationBar.tintColor = UIColor.white
        setupDelegates()
        configureUI()
        animationRegister()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    @IBAction func createAccountAction(_ sender: Any) {
        let username = usernameTextField.text
        let password = passwordTextField.text
        let confirmPassword = confirmationPasswordTextField.text
        interactor?.createNewUser(username: username!, password: password!, confirmPassword: confirmPassword!)
    }
}

extension RegisterViewController {

    private func setupDelegates() {
        let viewController = self
        let presenter = RegisterPresenter()
        let interactor = RegisterInteractor()
        let router = RegisterRouter()
        viewController.interactor = interactor
        viewController.router = router
        presenter.registerVCDelegate = viewController
        interactor.presenterDelegate = presenter
        router.registerVC = viewController
    }

    private func configureUI() {
        //TextField
        usernameTextField.frame = CGRect(x: -view.frame.size.width, y: 170, width: view.frame.size.width * 0.75, height: 30)
        usernameTextField.placeholder = "username"
        passwordTextField.frame = CGRect(x: -view.frame.size.width, y: usernameTextField.frame.maxY + 30, width: view.frame.size.width * 0.75, height: 30)
        passwordTextField.placeholder = "password"
        confirmationPasswordTextField.frame = CGRect(x: -view.frame.size.width, y: passwordTextField.frame.maxY + 30, width: view.frame.size.width * 0.75, height: 30)
        confirmationPasswordTextField.placeholder = "Confirm password"

        //Activity Indicator Loading
        activityIndicator.frame = CGRect(x: (view.frame.size.width / 2) - 20, y: view.frame.size.height / 2, width: 40, height: 40)
        activityIndicator.isHidden = true
        activityIndicator.color = UIColor.greenTVmaze
        view.addSubview(activityIndicator)
    }

    private func animationRegister() {
        usernameTextField.animateTextFieldToCenter(with: 2)
        passwordTextField.animateTextFieldToCenter(with: 2)
        confirmationPasswordTextField.animateTextFieldToCenter(with: 2)
    }

    private func blockUIelements(bool: Bool) {
        usernameTextField.isEnabled = !bool
        passwordTextField.isEnabled = !bool
        confirmationPasswordTextField.isEnabled = !bool
        createAccountButton.isEnabled = !bool
    }
}

extension RegisterViewController: RegisterDisplayLogic {

    func showLoader(bool: Bool) {
        switch bool {
        case true:
            DispatchQueue.main.async {
                self.blockUIelements(bool: true)
                self.activityIndicator.isHidden = false
                self.activityIndicator.startAnimating()
            }
        case false:
            DispatchQueue.main.async {
                self.blockUIelements(bool: false)
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
            }
        }
    }

    func showUserCreated(message: String) {
        let userCreatedAlert = UIAlertController(title: "User Created", message: message, preferredStyle: .alert)
        let acceptAction = UIAlertAction(title: "OK", style: .default) { (action) in
            self.showLogin()
        }
        userCreatedAlert.addAction(acceptAction)
        present(userCreatedAlert, animated: true, completion: nil)
    }

    func showError(message: String) {
        let errorAlert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let acceptAction = UIAlertAction(title: "OK", style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        errorAlert.addAction(acceptAction)
        present(errorAlert, animated: true, completion: nil)
    }

    func showLogin() {
        router?.showLogin()
    }
}

