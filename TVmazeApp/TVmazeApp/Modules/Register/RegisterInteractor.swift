//
//  RegisterInteractor.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import Foundation

protocol RegisterInteractorBusinessLogic {
    func createNewUser(username: String, password: String, confirmPassword: String)
}

class RegisterInteractor: RegisterInteractorBusinessLogic{

    var presenterDelegate: RegisterPresenterLogic?
    var worker: RegisterWorker?

    func createNewUser(username: String, password: String, confirmPassword: String) {
        presenterDelegate?.showLoader(bool: true)
        if !username.isEmpty && !password.isEmpty && !confirmPassword.isEmpty && password == confirmPassword {
            worker = RegisterWorker()
            worker?.createUser(username: username, password: password, onSuccess: { (success) in
                self.presenterDelegate?.creatingUSerResponse(isSuccess: success)
            }, onFailed: { (fail) in
                self.presenterDelegate?.creatingUSerResponse(isSuccess: fail)
            })
        } else {
            presenterDelegate?.creatingUSerResponse(isSuccess: false)
        }
        worker = nil
        presenterDelegate?.showLoader(bool: false)
    }
}
