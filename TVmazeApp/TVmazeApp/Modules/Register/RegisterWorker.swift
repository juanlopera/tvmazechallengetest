//
//  RegisterWorker.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import UIKit
import CoreData

class RegisterWorker {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    func createUser(username: String, password: String, onSuccess: @escaping(Bool)->Void, onFailed: @escaping(Bool)->Void) {
        let managedContext = appDelegate.persistentContainer.viewContext
        let userEntity = NSEntityDescription.entity(forEntityName: Constants.CoreData.UserEntity.userEntity, in: managedContext)!
        let userObject = NSManagedObject(entity: userEntity, insertInto: managedContext)
        userObject.setValue(username, forKey: Constants.CoreData.UserEntity.usernameAttribute)
        userObject.setValue(password, forKey: Constants.CoreData.UserEntity.passwordAttribute)
        do {
            try managedContext.save()
            onSuccess(true)
        } catch {
            onFailed(false)
        }
    }
}
