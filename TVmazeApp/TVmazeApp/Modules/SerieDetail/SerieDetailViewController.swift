//
//  SerieDetailViewController.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import UIKit

protocol SerieDetailVCDisplayLogic: class {
    func showLoader(bool: Bool)
    func showErrorRequest(message: String)
    func showEpisodes(episodes: [EpisodeViewModel])
    func showSaveInFavorites(message: String)
}

class SerieDetailViewController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var headerContainer: UIView!
    @IBOutlet weak var serieImage: UIImageView!
    @IBOutlet weak var serieNameLabel: UILabel!
    @IBOutlet weak var serieDetailTableView: UITableView!
    


    //MARK:- Properties
    //This is because I want to avoid have to initializate a SerieViewModel class
    var serieViewModel: [SerieViewModel] = [SerieViewModel]()
    var userModel: UserModel?
    private var episodes: [EpisodeViewModel] = [EpisodeViewModel]()
    private let activityIndicator = UIActivityIndicatorView()
    private let backButton = UIBarButtonItem()
    private let addToFavoritesButton = UIBarButtonItem()
    private let genericCellNib = UINib(nibName: Constants.SerieDetailTableView.NIB.genericCellNibName, bundle: nil)
    private let episodeCellNib = UINib(nibName: Constants.SerieDetailTableView.NIB.episodeCellNibName, bundle: nil)

    private var interactor: SerieDetailInteractorBusinessLogic?
    private var router: SerieDetailRouterLogic?

    private let datasourceTableView = DatasourceSerieDetailTableView()
    private let delegateTableView = DelegateSerieDetailTableView()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = UIColor.greenTVmaze
        self.navigationController?.navigationBar.tintColor = UIColor.white
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        addToFavoritesButton.title = "Add Favorite"
        addToFavoritesButton.target = self
        addToFavoritesButton.action = #selector(addFavorite)
        self.navigationItem.rightBarButtonItem = addToFavoritesButton
        setupDelegates()
        configureUI()
        registerCell()
        interactor?.fetchEpisodes(by: serieViewModel[0].id)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.Segue.serieDetailToEpisodeDetail {
            if let episodeDetailVC = segue.destination as? EpisodeDetailViewController {
                episodeDetailVC.episodeViewModel = sender as! EpisodeViewModel
            }
        }
    }

}

extension SerieDetailViewController {

    private func setupDelegates() {
        let viewController = self
        let presenter = SerieDetailPresenter()
        let interactor = SerieDetailInteractor()
        let router = SerieDetailRouter()
        viewController.interactor = interactor
        viewController.router = router
        presenter.serieDetailVCDelegate = viewController
        interactor.presenter = presenter
        router.SerieDetailVC = viewController

        //Table view
        delegateTableView.delegate = self
        serieDetailTableView.dataSource = datasourceTableView
        serieDetailTableView.delegate = delegateTableView
    }

    
    private func configureUI() {
        //Set properties
        serieImage.image = UIImage(data: serieViewModel[0].image)
        serieNameLabel.text = serieViewModel[0].name

        //Header Container
        headerContainer.layer.borderWidth = 1
        headerContainer.layer.borderColor = UIColor.grayLineTVmaze.cgColor
        headerContainer.layer.cornerRadius = 5
        headerContainer.layer.masksToBounds = false
        headerContainer.layer.shadowColor = UIColor.grayTVmaze.cgColor
        headerContainer.layer.shadowOpacity = 0.8
        headerContainer.layer.shadowRadius = 5
        headerContainer.layer.shadowOffset = CGSize(width: 5, height: -4)

        //TableView
        serieDetailTableView.tableFooterView = UIView()
        serieDetailTableView.estimatedRowHeight = 80
        serieDetailTableView.rowHeight = UITableView.automaticDimension
        datasourceTableView.series = self.serieViewModel
        serieDetailTableView.reloadData()

        //Activity Indicator Loading
        activityIndicator.frame = CGRect(x: (view.frame.size.width / 2) - 20, y: view.frame.size.height / 2, width: 40, height: 40)
        activityIndicator.isHidden = true
        activityIndicator.color = UIColor.greenTVmaze
        activityIndicator.backgroundColor = UIColor.grayTVmaze
        view.addSubview(activityIndicator)
    }

    private func registerCell() {
        serieDetailTableView.register(genericCellNib, forCellReuseIdentifier: Constants.SerieDetailTableView.CellIdentifiers.genericSerieCell)
        serieDetailTableView.register(episodeCellNib, forCellReuseIdentifier: Constants.SerieDetailTableView.CellIdentifiers.episodeDetailCell)
    }

    private func blockUIelements(bool: Bool) {
        serieDetailTableView.isScrollEnabled = !bool
        addToFavoritesButton.isEnabled = !bool
    }

    @objc private func addFavorite() {
        interactor?.addToFavorite(serie: serieViewModel, in: userModel!)
    }
}

extension SerieDetailViewController: SerieDetailVCDisplayLogic {
    func showLoader(bool: Bool) {
        switch bool {
        case true:
            DispatchQueue.main.async {
                self.blockUIelements(bool: true)
                self.activityIndicator.isHidden = false
                self.activityIndicator.startAnimating()
            }
        case false:
            DispatchQueue.main.async {
                self.blockUIelements(bool: false)
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
            }
        }
    }

    func showErrorRequest(message: String) {
        let errorAlert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let acceptAction = UIAlertAction(title: "OK", style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        errorAlert.addAction(acceptAction)
        present(errorAlert, animated: true, completion: nil)
    }

    func showEpisodes(episodes: [EpisodeViewModel]) {
        self.episodes = episodes
        serieViewModel[0].episodes = self.episodes
        datasourceTableView.series[0].episodes = self.episodes
        DispatchQueue.main.async {
            self.serieDetailTableView.reloadData()
        }
    }

    func showSaveInFavorites(message: String) {
        let successAlert = UIAlertController(title: "Success!", message: message, preferredStyle: .alert)
        let acceptAction = UIAlertAction(title: "OK", style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        successAlert.addAction(acceptAction)
        present(successAlert, animated: true, completion: nil)
    }
}

extension SerieDetailViewController: DelegateSeriesDetailTableProtocol {
    func episodeSelected(index: Int) {
        if let episode = serieViewModel[0].episodes {
            router?.showEpisodeDetail(episodeDetail: episode[index])
        }
    }
}
