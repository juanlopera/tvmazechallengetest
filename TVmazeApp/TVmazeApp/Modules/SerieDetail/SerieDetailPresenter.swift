//
//  SerieDetailPresenter.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/4/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import Foundation

protocol SerieDetailPresenterPresenterLogic: class {
    func showLoader(bool: Bool)
    func episodesResponsePresenter(episodes: [EpisodeViewModel])
    func saveInFavoriteResponsePresenter()
    func onResquestError(error: GenericsError)
    func onResponseError(error: GenericsError)
    func onShowNumberError()
}


class SerieDetailPresenter: SerieDetailPresenterPresenterLogic {

    weak var serieDetailVCDelegate: SerieDetailVCDisplayLogic?
    
    func showLoader(bool: Bool) {
        serieDetailVCDelegate?.showLoader(bool: bool)
    }

    func onResponseError(error: GenericsError) {
        let errorMessage: String = "Was a error in the format response with our system, please try again or contact TV Maze API for more information"
        serieDetailVCDelegate?.showErrorRequest(message: errorMessage)
        serieDetailVCDelegate?.showLoader(bool: false)
    }

    func onResquestError(error: GenericsError) {
        let errorMessage: String = "There is a problem with TV Maze system, please go to http://www.tvmaze.com/api   for more information"
        serieDetailVCDelegate?.showErrorRequest(message: errorMessage)
        serieDetailVCDelegate?.showLoader(bool: false)
    }

    func onShowNumberError() {
        let errorMessage: String = "The episode is invalid or not available in the moment"
        serieDetailVCDelegate?.showErrorRequest(message: errorMessage)
        serieDetailVCDelegate?.showLoader(bool: false)
    }

    func episodesResponsePresenter(episodes: [EpisodeViewModel]) {
        if episodes.count > 0 {
            var episodesFormated = episodes
            for i in 0...episodesFormated.count - 1 {
                episodesFormated[i].summary = Utils.fromHTMLFormatter(string: episodesFormated[i].summary)
            }
            serieDetailVCDelegate?.showLoader(bool: false)
            serieDetailVCDelegate?.showEpisodes(episodes: episodesFormated)
        } else {
            serieDetailVCDelegate?.showLoader(bool: false)
            serieDetailVCDelegate?.showErrorRequest(message: "Episodes not found or not available in the moment, for more information contact TV Maze support")
        }
    }

    func saveInFavoriteResponsePresenter() {
        serieDetailVCDelegate?.showLoader(bool: false)
        serieDetailVCDelegate?.showSaveInFavorites(message: "Serie saved in favorite section")
    }
}
