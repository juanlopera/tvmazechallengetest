//
//  EpisodeTableViewCell.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/4/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import UIKit

class EpisodeTableViewCell: UITableViewCell {

    //MARK:- IBOutlet
    @IBOutlet weak var leftContainerView: UIView!
    @IBOutlet weak var episodeNumberLabel: UILabel!
    @IBOutlet weak var splitLineView: UIView!
    @IBOutlet weak var numberEpisodeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var episodeNameLabel: UILabel!
    @IBOutlet weak var episodeSeasonLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func prepareForReuse() {
        leftContainerView.isHidden = true
        episodeNumberLabel.text = ""
        numberEpisodeLabel.text = ""
        splitLineView.isHidden = true
        nameLabel.text = ""
        episodeNameLabel.text = ""
        episodeSeasonLabel.text = ""
    }
    
}
