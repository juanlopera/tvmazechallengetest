//
//  GenericDetailTableViewCell.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/4/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import UIKit

class GenericDetailTableViewCell: UITableViewCell {

    //MARK:- IBOutlet
    @IBOutlet weak var genericTitleLabel: UILabel!
    @IBOutlet weak var genericDetailLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func prepareForReuse() {
        genericTitleLabel.text = ""
        genericDetailLabel.text = ""
    }
}
