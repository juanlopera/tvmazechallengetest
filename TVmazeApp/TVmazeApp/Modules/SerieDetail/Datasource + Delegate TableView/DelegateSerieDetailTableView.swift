//
//  DelegateSerieDetailTableView.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/4/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import UIKit

protocol DelegateSeriesDetailTableProtocol {
    func episodeSelected(index: Int)
}

class DelegateSerieDetailTableView: NSObject, UITableViewDelegate {

    var delegate: DelegateSeriesDetailTableProtocol?

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 3 {
            delegate?.episodeSelected(index: indexPath.row)
        }
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 3 {
            return 40.0
        } else {
            return 0.0
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 3 {
            let headerView = UIView()
            headerView.frame = CGRect(x: 0, y: 0, width: 200.0, height: 30)
            headerView.backgroundColor = UIColor.grayTVmaze
            let episodeLabel = UILabel()
            episodeLabel.frame = CGRect(x: 16, y: 0, width: 200.0, height: 30)
            episodeLabel.text = "Episodes"
            episodeLabel.font = UIFont.boldSystemFont(ofSize: 20)
            headerView.addSubview(episodeLabel)
            return headerView
        } else {
            return nil
        }
    }
}
