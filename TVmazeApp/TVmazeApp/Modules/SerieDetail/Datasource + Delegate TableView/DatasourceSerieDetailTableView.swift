//
//  DatasourceSerieDetailTableView.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/4/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import UIKit

class DatasourceSerieDetailTableView: NSObject, UITableViewDataSource {

    var series: [SerieViewModel] = [SerieViewModel]()

    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 3:
            if series.count > 0, let episodes = series[0].episodes {
                return episodes.count
            } else {
                return 0
            }
        default:
            return 1
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if series.count > 0 {
            if indexPath.section == 3 {
                if let episodes = series[0].episodes {
                    let cell = tableView.dequeueReusableCell(withIdentifier: Constants.SerieDetailTableView.CellIdentifiers.episodeDetailCell, for: indexPath) as! EpisodeTableViewCell
                    cell.leftContainerView.isHidden = false
                    cell.numberEpisodeLabel.text = episodes[indexPath.row].episodeNumber
                    cell.episodeNumberLabel.text = "Number"
                    cell.splitLineView.isHidden = false
                    cell.nameLabel.text = "Epidose name"
                    cell.episodeNameLabel.text = episodes[indexPath.row].name
                    cell.episodeSeasonLabel.text = "Season: \(episodes[indexPath.row].season)"
                    cell.accessoryType = .disclosureIndicator
                    return cell
                } else {
                    return UITableViewCell()
                }
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: Constants.SerieDetailTableView.CellIdentifiers.genericSerieCell, for: indexPath) as! GenericDetailTableViewCell
                switch indexPath.section {
                case 0:
                    cell.genericTitleLabel.text = "Days"
                    if series[0].availableDays.count > 0 {
                        cell.genericDetailLabel.text = getStringDays(days: series[0].availableDays)
                    } else {
                        cell.genericDetailLabel.text = "this information will be available soon"
                    }
                    return cell
                case 1:
                    cell.genericTitleLabel.text = "Genres"
                    if series[0].genres.count > 0 {
                        cell.genericDetailLabel.text = getStringGenres(genres: series[0].genres)
                    } else {
                        cell.genericDetailLabel.text = "this information will be available soon"
                    }
                    return cell
                default:
                    cell.genericTitleLabel.text = "Summary"
                    cell.genericDetailLabel.text = series[0].summary
                    return cell
                }
            }
        } else {
            return UITableViewCell()
        }
    }
}

extension DatasourceSerieDetailTableView {
    private func getStringDays(days: [String]) -> String {
        var daysString = ""
        for i in 0...days.count - 1 {
            if i == 0{
                daysString = days[i] + " "
            } else {
                daysString = ", " + days[i]
            }
        }
        return daysString
    }

    private func getStringGenres(genres: [String]) -> String {
        return getStringDays(days: genres)
    }
}
