//
//  SerieDetailWorker.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/4/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import UIKit
import CoreData

class SerieDetailWorker {
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let defaultSession = URLSession(configuration: .default)
    private var simpleTaskRequest: URLSessionDataTask?
    
    func fetchEpisode(by number: String, onResult: @escaping(Result<[EpisodeModel], GenericsError>) -> Void){
        simpleTaskRequest?.cancel()

        if var urlComponents = URLComponents(string: "http://api.tvmaze.com/shows/\(number)/episodes") {
            guard let url = urlComponents.url else {
                onResult(.failure(.onRequest))
                return
            }
            simpleTaskRequest = defaultSession.dataTask(with: url, completionHandler: { (data, response, error) in
                defer {
                    self.simpleTaskRequest = nil
                }
                guard (error == nil) else {
                    onResult(.failure(.onRequest))
                    return
                }

                if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                    do {
                        let episodes = try JSONDecoder().decode([EpisodeModel].self, from: data)
                        onResult(.success(episodes))
                    } catch {
                        onResult(.failure(.onResponse))
                    }
                } else {
                    onResult(.failure(.onRequest))
                }
            })
            simpleTaskRequest?.resume()
        }
    }

    func addToFavorites(serie: SerieViewModel, in user: UserModel, completionHandler: @escaping(Bool) -> Void) {
        let manageContext = appDelegate.persistentContainer.viewContext
        let userEntity = User(context: manageContext)
        let fetch = NSFetchRequest<User>(entityName: Constants.CoreData.UserEntity.userEntity)
        fetch.predicate = NSPredicate(format: "username == %@", user.name!)
        do {
            _ = try manageContext.fetch(fetch)
        } catch  {
            completionHandler(false)
        }

        let serieEntityDescription = NSEntityDescription.entity(forEntityName: Constants.CoreData.SerieEntity.serieEntity, in: manageContext)
        let serieEntity = Series(entity: serieEntityDescription!, insertInto: manageContext)
        userEntity.username = user.name
        userEntity.password = user.password
        serieEntity.seriesUser = userEntity
        serieEntity.id = serie.id
        serieEntity.name = serie.name
        serieEntity.summary = serie.summary
        serieEntity.image = NSData()
        do {
            try manageContext.save()
            completionHandler(true)
        } catch {
            completionHandler(false)
        }
    }
}
