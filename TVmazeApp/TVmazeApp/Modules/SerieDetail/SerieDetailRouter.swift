//
//  SerieDetailRouter.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/4/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import Foundation

protocol SerieDetailRouterLogic {
    func showEpisodeDetail(episodeDetail: EpisodeViewModel)
}

class SerieDetailRouter: SerieDetailRouterLogic {

    var SerieDetailVC: SerieDetailViewController?
    
    func showEpisodeDetail(episodeDetail: EpisodeViewModel) {
        SerieDetailVC?.performSegue(withIdentifier: Constants.Segue.serieDetailToEpisodeDetail, sender: episodeDetail)
    }
}
