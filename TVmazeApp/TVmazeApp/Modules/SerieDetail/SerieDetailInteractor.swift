//
//  SerieDetailInteractor.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/4/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import Foundation

protocol SerieDetailInteractorBusinessLogic {
    var presenter: SerieDetailPresenterPresenterLogic? { get }
    func fetchEpisodes(by showNumber: String)
    func addToFavorite(serie: [SerieViewModel], in user: UserModel)
}

class SerieDetailInteractor: SerieDetailInteractorBusinessLogic {

    var presenter: SerieDetailPresenterPresenterLogic?
    lazy var worker: SerieDetailWorker = SerieDetailWorker()
    
    func fetchEpisodes(by showNumber: String) {
        presenter?.showLoader(bool: true)
        if showNumber.isEmpty {
            presenter?.onShowNumberError()
        } else {
            worker.fetchEpisode(by: showNumber) { (result) in
                switch result {
                case .success(let episodesResponse):
                    self.presenter?.episodesResponsePresenter(episodes: self.caseShowEpisodes(episodeResponse: episodesResponse))
                case .failure(let error):
                    self.caseError(error: error)
                }
            }
        }
    }

    func addToFavorite(serie: [SerieViewModel], in user: UserModel) {
        presenter?.showLoader(bool: true)
        if serie.count > 0 {
            worker.addToFavorites(serie: serie[0], in: user, completionHandler: { (response) in
                switch response {
                case true:
                    self.presenter?.saveInFavoriteResponsePresenter()
                case false:
                    self.presenter?.onResquestError(error: .onRequest)
                }
            })
        } else {
            presenter?.onResquestError(error: .onRequest)
        }
    }
}

extension SerieDetailInteractor {
    private func caseError(error: GenericsError) {
        switch error {
        case .onRequest:
            self.presenter?.onResquestError(error: .onRequest)
        case .onResponse:
            self.presenter?.onResquestError(error: .onResponse)
        }
    }

    private func caseShowEpisodes(episodeResponse: [EpisodeModel]) -> [EpisodeViewModel] {
        let episodeViewModel = episodeResponse.map { (episodeModel) -> EpisodeViewModel in
            let episodeNumber = String(episodeModel.number)
            let epidoseSeasson = String(episodeModel.season)
            let imgURL = URL(string: episodeModel.image.original)
            let imgData = try? Data(contentsOf: imgURL!)
            let episodeVM = EpisodeViewModel(name: episodeModel.name, episodeNumber: episodeNumber, season: epidoseSeasson, summary: episodeModel.summary, episodeImage: imgData ?? Data())
            return episodeVM
        }

        return episodeViewModel
    }
}
