//
//  Constants.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import Foundation

struct Constants {
    struct Segue {
        static let loginToRegister = "loginToRegisterSegue"
        static let loginToHome = "loginToHome"
        static let homeToSerieDetail = "homeToSerieDetail"
        static let serieDetailToEpisodeDetail = "serieDetailToEpisodeDetail"
        static let homeToFavorites = "homeToFavorites"
        static let favoriteToEpisodeDetail = "favoriteToEpisode"
    }

    struct CoreData {
        struct UserEntity {
            static let userEntity = "User"
            static let usernameAttribute = "username"
            static let passwordAttribute = "password"
        }
        struct SerieEntity {
            static let serieEntity = "Series"
            static let idAttribute = "id"
            static let imageAttribute = "image"
            static let summaryAttribute = "summary"
            static let nameAttribute = "name"
        }
    }

    struct SeriesCollectionView {
        struct CellIdentifiers {
            static let seriesCellIdentifier = "seriesCell"
        }

        struct NIB {
            static let seriesNibName = "SerieCollectionCell"
        }
    }

    struct FavoriteSerieCollectionView {
        struct CellIdentifier {
            static let favoriteSerieCellIdentifier = "favoriteSerieCell"
        }

        struct NIB {
            static let FavoriteSerieCellNibName = "FavoriteSerieCollectionViewCell"
        }
    }

    struct SerieDetailTableView {
        struct CellIdentifiers {
            static let genericSerieCell = "genericDetailCell"
            static let episodeDetailCell = "detailEpisodeCell"
        }

        struct NIB {
            static let genericCellNibName = "GenericDetailTableViewCell"
            static let episodeCellNibName = "EpisodeTableViewCell"
        }
    }
}
