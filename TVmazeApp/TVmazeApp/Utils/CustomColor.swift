//
//  CustomColor.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/3/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import UIKit

extension UIColor {
    static var greenTVmaze: UIColor = UIColor(red: 60/255, green: 141/255, blue: 132/255, alpha: 1.0)

    static var grayTVmaze: UIColor = UIColor(red: 225/255, green: 226/255, blue: 227/255, alpha: 1.0)

    static var grayLineTVmaze: UIColor = UIColor(red: 108/255, green: 108/255, blue: 108/255, alpha: 1.0)

    static var redDeleteTVmaze : UIColor = UIColor(red: 218/255, green: 68/255, blue: 46/255, alpha: 1.0)
}


