//
//  Utils.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 9/10/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import Foundation

struct Utils {
    static func fromHTMLFormatter(string: String) -> String {
        var newString = string
        let char_dictionary = [
            "&amp;" : "&",
            "&lt;" : "<",
            "&gt;" : ">",
            "&quot;" : "\"",
            "&apos;" : "'",
            "<b>" : "",
            "<p>" : "",
            "</p>" : "",
            "</b>" : ""
        ]

        for (escaped_char, unescaped_char) in char_dictionary {
            newString = newString.replacingOccurrences(of: escaped_char, with: unescaped_char, options: NSString.CompareOptions.literal, range: nil)
        }
        return newString
    }
}
