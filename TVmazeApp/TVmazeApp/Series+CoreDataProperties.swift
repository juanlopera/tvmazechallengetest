//
//  Series+CoreDataProperties.swift
//  TVmazeApp
//
//  Created by Juan David Lopera Lopez on 8/4/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//
//

import Foundation
import CoreData


extension Series {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Series> {
        return NSFetchRequest<Series>(entityName: "Series")
    }

    @NSManaged public var name: String?
    @NSManaged public var id: String?
    @NSManaged public var image: NSData?
    @NSManaged public var summary: String?
    @NSManaged public var seriesUser: User?

}
