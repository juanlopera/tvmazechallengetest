//
//  TVmazeAppTests.swift
//  TVmazeAppTests
//
//  Created by Juan David Lopera Lopez on 8/2/19.
//  Copyright © 2019 Juan David Lopera Lopez. All rights reserved.
//

import XCTest
@testable import TVmazeApp

class TVmazeAppTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testValidCallToTVmazeApiShows() {
        let defaultSession = URLSession(configuration: .default)
        let url = URL(string: "http://api.tvmaze.com/shows")
        let promise = expectation(description: "Status code received: 200")
        let dataTask = defaultSession.dataTask(with: url!) { data, response, error in

            if let error = error {
                XCTFail("Error: \(error.localizedDescription)")
                return
            } else if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                if statusCode == 200 {
                    promise.fulfill()
                } else {
                    XCTFail("Status code received: \(statusCode)")
                }
            }
        }
        dataTask.resume()
        wait(for: [promise], timeout: 10)
    }

    func testValidCallToTVmazeApiEpisodes() {
        let defaultSession = URLSession(configuration: .default)
        let episodeNumber = "1"
        let url = URL(string: "http://api.tvmaze.com/shows/\(episodeNumber)/episodes")
        let promise = expectation(description: "Status code received: 200")
        let dataTask = defaultSession.dataTask(with: url!) { data, response, error in

            if let error = error {
                XCTFail("Error: \(error.localizedDescription)")
                return
            } else if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                if statusCode == 200 {
                    promise.fulfill()
                } else {
                    XCTFail("Status code received: \(statusCode)")
                }
            }
        }
        dataTask.resume()
        wait(for: [promise], timeout: 10)
    }
}
